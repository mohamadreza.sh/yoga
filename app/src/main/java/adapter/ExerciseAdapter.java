package adapter;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.example.yogatraining.R;
import com.example.yogatraining.ShowExercise;

import java.io.ByteArrayOutputStream;
import java.util.ArrayList;

import Interface.ItemClickListener;
import database.YogaDatabase;
import model.Exercise;

public class ExerciseAdapter extends RecyclerView.Adapter<ExerciseViewHolder> {
    private ArrayList<Exercise> exerciseList;
    private Exercise exercise;
    private Context context;
    private YogaDatabase database;


    public ExerciseAdapter(ArrayList<Exercise> exerciseList, Context context) {
        this.exerciseList = exerciseList;
        this.context = context;
    }

    @NonNull
    @Override
    public ExerciseViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
        View view = layoutInflater.inflate(R.layout.exercise_list, parent, false);
        return new ExerciseViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull final ExerciseViewHolder holder, final int position) {
        holder.text.setText(exerciseList.get(position).getName());
        holder.text.setAllCaps(true);
        holder.text.setTextSize(15);
        Glide.with(context).load(R.drawable.bookmark).into(holder.imgFav);
        Glide.with(context).load(exerciseList.get(position).getImage()).into(holder.images);


        holder.imgFav.setOnClickListener(new View.OnClickListener() {


            @Override
            public void onClick(View v) {
                String s = exerciseList.get(position).getName();
                Bitmap b = exerciseList.get(position).getImage();
                int f = exerciseList.get(position).getFav();
                int i = exerciseList.get(position).getId();
                exercise = new Exercise();
                exercise.setName(s);
                exercise.setImage(b);
                exercise.setFav(f);
                exercise.setId(exerciseList.get(position).getId());
                if (exercise.getFav() == 0) {
                    database = new YogaDatabase(context);
                    database.saveFavExercise(exercise);
                    Toast.makeText(context, "Exercise has been saved", Toast.LENGTH_SHORT).show();
                    database.changeFav(exerciseList.get(position).getId(), 1);
                    exerciseList.get(position).setFav(1);


                } else {
                    //  holder.imgFav.setEnabled(false);
                    Toast.makeText(context, "Exercise has been  already saved", Toast.LENGTH_SHORT).show();
                }

            }
        });


        holder.setItemClickListener(new ItemClickListener() {
            @Override
            public void onclick(View view, int position) {
                Intent intent = new Intent(context, ShowExercise.class);
                Bitmap bitmap = exerciseList.get(position).getImage();
                if (bitmap != null) {
                    ByteArrayOutputStream bs = new ByteArrayOutputStream();
                    bitmap.compress(Bitmap.CompressFormat.PNG, 100, bs);
                    intent.putExtra("image", bs.toByteArray());
                }
                //  intent.putExtra("image", exerciseList.get(position).getImage());
                intent.putExtra("name", exerciseList.get(position).getName());
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                context.startActivity(intent);
            }
        });

    }

    @Override
    public int getItemCount() {
        // return (exerciseList == null) ? 0 : exerciseList.size();
        return exerciseList.size();
    }
}

class ExerciseViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
    public ImageView images;
    public TextView text;
    public ImageView imgFav;

    private ItemClickListener itemClickListener;

    public ExerciseViewHolder(@NonNull View itemView) {
        super(itemView);
        images = itemView.findViewById(R.id.ex_img);
        text = itemView.findViewById(R.id.ex_name);
        imgFav = itemView.findViewById(R.id.imgFav);

        itemView.setOnClickListener(this);
    }

    public void setItemClickListener(ItemClickListener itemClickListener) {
        this.itemClickListener = itemClickListener;
    }

    @Override
    public void onClick(View v) {
        itemClickListener.onclick(v, getAdapterPosition());

    }
}
