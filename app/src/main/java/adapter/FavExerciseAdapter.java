package adapter;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.example.yogatraining.R;
import com.example.yogatraining.ShowExercise;

import java.io.ByteArrayOutputStream;
import java.util.ArrayList;

import Interface.ItemClickListener;
import database.YogaDatabase;
import model.Exercise;

public class FavExerciseAdapter extends RecyclerView.Adapter<FavExerciseViewHolder> {
    int i;
    private Context context;
    private ArrayList<Exercise> exerciseList;


    public FavExerciseAdapter(ArrayList<Exercise> exerciseList, Context context) {
        this.exerciseList = exerciseList;
        this.context = context;
    }

    @NonNull
    @Override
    public FavExerciseViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
        View view = layoutInflater.inflate(R.layout.exercise_list, parent, false);
        return new FavExerciseViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull final FavExerciseViewHolder holder, final int position) {
        holder.text.setText(exerciseList.get(position).getName());
        Glide.with(context).load(R.drawable.trash).into(holder.deleteImage);
        Glide.with(context).load(exerciseList.get(position).getImage()).into(holder.images);


        holder.deleteImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // i = exerciseList.get(position).getId();
                get(exerciseList.get(position).getId(), position);


//        exerciseList.remove(position);
//        notifyItemRemoved(position);
                //exerciseList.remove(position);
                // exerciseList.remove(exerciseList.get(position));
                //notifyItemRemoved(position);


            }
        });

        holder.setItemClickListener(new ItemClickListener() {
            @Override
            public void onclick(View view, int position) {
                Intent intent = new Intent(context, ShowExercise.class);
                Bitmap bitmap = exerciseList.get(position).getImage();
                if (bitmap != null) {
                    ByteArrayOutputStream bs = new ByteArrayOutputStream();
                    bitmap.compress(Bitmap.CompressFormat.PNG, 100, bs);
                    intent.putExtra("image", bs.toByteArray());
                }
                intent.putExtra("name", exerciseList.get(position).getName());
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                context.startActivity(intent);
            }
        });

    }

    public void get(final int id, final int position) {
        YogaDatabase database = new YogaDatabase(context);
        database.changeFav(exerciseList.get(position).getId(), 0);
        exerciseList.get(position).setFav(0);
        database.deleteContact(id);
        exerciseList.remove(position);
        notifyItemRemoved(position);
        notifyItemRangeChanged(position, exerciseList.size());
        Toast.makeText(context, "item removed", Toast.LENGTH_SHORT).show();
        Log.d("log", "position" + i);

    }


    @Override
    public int getItemCount() {
        return exerciseList.size();
    }
}


class FavExerciseViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

    public ImageView images;
    public ImageView deleteImage;
    public TextView text;

    private ItemClickListener itemClickListener;

    public FavExerciseViewHolder(@NonNull View itemView) {
        super(itemView);
        images = itemView.findViewById(R.id.ex_img);
        deleteImage = itemView.findViewById(R.id.imgFav);
        text = itemView.findViewById(R.id.ex_name);


        itemView.setOnClickListener(this);
    }


    public void setItemClickListener(ItemClickListener itemClickListener) {
        this.itemClickListener = itemClickListener;
    }

    @Override
    public void onClick(View v) {
        itemClickListener.onclick(v, getAdapterPosition());
    }

}

