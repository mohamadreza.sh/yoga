package com.example.yogatraining;

import android.annotation.SuppressLint;
import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Build;

import androidx.annotation.RequiresApi;
import androidx.core.app.NotificationCompat;

import static android.content.Context.NOTIFICATION_SERVICE;

public class AlarmNotificationReceiver extends BroadcastReceiver {
    public static final String NOTIFICATION_CHANEL_ID = "alarm";
    int id;


    @RequiresApi(api = Build.VERSION_CODES.O)
    @Override
    public void onReceive(Context context, Intent intent) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            if (!new SpManager(context).isChanelCreated()) {
                createChanel(context);
            }

            notification(context, intent);
        } else {

            notification(context, intent);
        }


    }

    @RequiresApi(api = Build.VERSION_CODES.O)
    public void notification(Context context, Intent intent) {
        id = intent.getIntExtra("notificationId", 1);
        Intent intent1 = new Intent(context, SettingPage.class);
        PendingIntent pendingIntent = PendingIntent.getActivity(context, 0, intent1, 0);

        NotificationManager notificationManager = (NotificationManager) context.getSystemService(NOTIFICATION_SERVICE);

        Notification.Builder builder = new Notification.Builder(context, NOTIFICATION_CHANEL_ID);
        builder.setSmallIcon(R.mipmap.ic_launcher_round)
                .setContentTitle("it's time")
                .setContentText("time to training")
                .setWhen(System.currentTimeMillis())
                .setAutoCancel(true)
                .setContentIntent(pendingIntent)
                .setPriority(Notification.PRIORITY_MAX)
                .setDefaults(Notification.DEFAULT_ALL);
        notificationManager.notify(id, builder.build());

    }

    public void showNotification(Context context) {

        NotificationCompat.Builder builder = new NotificationCompat.Builder(context, NOTIFICATION_CHANEL_ID);
        builder.setAutoCancel(true)
                .setDefaults(Notification.DEFAULT_ALL)
                .setWhen(System.currentTimeMillis())
                .setSmallIcon(R.mipmap.ic_launcher_round)
                .setWhen(System.currentTimeMillis())
                .setContentTitle("It's time ")
                .setContentText("time to training")
                .setContentInfo("info");

        NotificationManager manager = (NotificationManager) context.getSystemService(NOTIFICATION_SERVICE);
        manager.notify(0, builder.build());

    }

    @RequiresApi(api = Build.VERSION_CODES.O)
    private void createChanel(Context context) {
        @SuppressLint("WrongConstant")
        NotificationChannel notificationChannel = new NotificationChannel(NOTIFICATION_CHANEL_ID, "alarm", NotificationManager.IMPORTANCE_MAX);
        notificationChannel.setDescription("My Alarm");
        notificationChannel.enableLights(true);
        notificationChannel.enableVibration(true);
        NotificationManager notificationManager = (NotificationManager) context.getSystemService(NOTIFICATION_SERVICE);
        notificationManager.createNotificationChannel(notificationChannel);
        new SpManager(context).setChancelCreated(true);

    }


}
