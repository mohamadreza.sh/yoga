package com.example.yogatraining;

import android.os.Bundle;

import androidx.appcompat.app.AppCompatActivity;

import com.prolificinteractive.materialcalendarview.CalendarDay;
import com.prolificinteractive.materialcalendarview.MaterialCalendarView;

import java.util.Date;
import java.util.HashSet;
import java.util.List;

import custom.WorkoutDoneDecoder;
import database.YogaDatabase;

public class Calender extends AppCompatActivity {
    MaterialCalendarView calender;
    HashSet<CalendarDay> list = new HashSet<>();

    YogaDatabase yogaDatabase;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_calender);
        calender = findViewById(R.id.calender);
        yogaDatabase = new YogaDatabase(this);

//        Date date = new Date();
//        SimpleDateFormat date_format = new SimpleDateFormat("yyyy-MM-dd");
//        try {
//            date = date_format.parse("2008-01-01");
//        } catch (ParseException e) {
//            e.printStackTrace();
//        }
        //get all workoutDay from database

        List<String> workoutDay = yogaDatabase.getWorkoutDays();
        list = new HashSet<>();
        for (String value : workoutDay) {

            list.add(CalendarDay.from(new Date(Long.parseLong(value))));
            calender.addDecorator(new WorkoutDoneDecoder(list, this));


        }

    }
}
