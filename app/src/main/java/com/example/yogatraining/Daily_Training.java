package com.example.yogatraining;

import android.graphics.Color;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import java.util.ArrayList;
import java.util.Calendar;

import database.YogaDatabase;
import me.zhanghai.android.materialprogressbar.MaterialProgressBar;
import model.Exercise;
import utils.Common;

public class Daily_Training extends AppCompatActivity {

    YogaDatabase yogaDatabase;
    int ex_id = 0;
    ArrayList<Exercise> exerciseList = new ArrayList<>();
    String level;
    private TextView ex_name;
    private MaterialProgressBar progressBar;
    private TextView timer;
    private ImageView ex_img;
    private LinearLayout layoutGetReady;
    private TextView txtGetReady;
    private TextView txtCountDown;
    private Button btnStart;

    CountDownTimer easyCountDownTimer = new CountDownTimer(Common.TIME_LIMIT_EASY, 1000) {
        @Override
        public void onTick(long millisUntilFinished) {
            timer.setText("" + (millisUntilFinished / 1000));
            timer.setTextColor(Color.parseColor(String.valueOf(R.color.colorPrimaryDark)));

        }

        @Override
        public void onFinish() {
            if (ex_id < exerciseList.size() - 1) {
                ex_id++;
                progressBar.setProgress(ex_id);
                timer.setText("");

                setExerciseInformation(ex_id);
                btnStart.setText("start");

            } else {
                showFinished();
            }

        }
    };
    CountDownTimer mediumCountDownTimer = new CountDownTimer(Common.TIME_LIMIT_MEDIUM, 1000) {
        @Override
        public void onTick(long millisUntilFinished) {
            timer.setText("" + (millisUntilFinished / 1000));

        }

        @Override
        public void onFinish() {
            if (ex_id < exerciseList.size() - 1) {
                ex_id++;
                progressBar.setProgress(ex_id);
                timer.setText("");

                setExerciseInformation(ex_id);
                btnStart.setText("start");

            } else {
                showFinished();
            }

        }
    };
    CountDownTimer hardCountDownTimer = new CountDownTimer(Common.TIME_LIMIT_HARD, 1000) {
        @Override
        public void onTick(long millisUntilFinished) {
            timer.setText("" + (millisUntilFinished / 1000));

        }

        @Override
        public void onFinish() {
            if (ex_id < exerciseList.size() - 1) {
                ex_id++;
                progressBar.setProgress(ex_id);
                timer.setText("");

                setExerciseInformation(ex_id);
                btnStart.setText("start");

            } else {
                showFinished();
            }

        }
    };
    CountDownTimer restCountDownTimer = new CountDownTimer(10000, 1000) {
        @Override
        public void onTick(long millisUntilFinished) {
            txtCountDown.setText("" + (millisUntilFinished / 1000));

        }

        @Override
        public void onFinish() {
            setExerciseInformation(ex_id);
            showExercise();
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_daily__training);
        setExerciseList();
        ex_name = findViewById(R.id.title);
        progressBar = findViewById(R.id.progressBar);
        timer = findViewById(R.id.timer);
        ex_img = findViewById(R.id.img_Detail);
        layoutGetReady = findViewById(R.id.layout_get_ready);
        txtGetReady = findViewById(R.id.txtGetReady);
        txtCountDown = findViewById(R.id.txtCountDown);
        btnStart = findViewById(R.id.btnStart);


        yogaDatabase = new YogaDatabase(this);

        progressBar.setMax(exerciseList.size());

        setExerciseInformation(ex_id);

        btnStart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (btnStart.getText().toString().toLowerCase().equals("start")) {

                    showGetReady();
                    btnStart.setText("done");

                } else if (btnStart.getText().toString().toLowerCase().equals("done")) {
                    if (yogaDatabase.getSettingMode() == 0) {
                        easyCountDownTimer.cancel();
                    } else if (yogaDatabase.getSettingMode() == 1) {
                        mediumCountDownTimer.cancel();
                    } else if (yogaDatabase.getSettingMode() == 2) {
                        hardCountDownTimer.cancel();
                    }

                    restCountDownTimer.cancel();
                    if (ex_id < exerciseList.size()) {
                        showRestTime();
                        ex_id++;
                        progressBar.setProgress(ex_id);
                        timer.setText("");
                    } else {
                        showFinished();
                    }

                } else {
                    if (yogaDatabase.getSettingMode() == 0) {
                        easyCountDownTimer.cancel();
                    } else if (yogaDatabase.getSettingMode() == 1) {
                        mediumCountDownTimer.cancel();
                    } else if (yogaDatabase.getSettingMode() == 2) {
                        hardCountDownTimer.cancel();

                    }
                    restCountDownTimer.cancel();
                    if (ex_id < exerciseList.size()) {
                        setExerciseInformation(ex_id);

                    } else {
                        showFinished();
                    }

                }
            }
        });

    }

    private void showRestTime() {
        ex_img.setVisibility(View.INVISIBLE);
        timer.setVisibility(View.INVISIBLE);
        btnStart.setVisibility(View.VISIBLE);
        btnStart.setText("skip");
        layoutGetReady.setVisibility(View.VISIBLE);

        restCountDownTimer.start();

        txtGetReady.setText("Rest Time");
    }

    private void showGetReady() {
        ex_img.setVisibility(View.INVISIBLE);

        btnStart.setVisibility(View.INVISIBLE);
        timer.setVisibility(View.VISIBLE);

        layoutGetReady.setVisibility(View.VISIBLE);

        txtGetReady.setText("Get Ready");
        new CountDownTimer(6000, 1000) {
            @Override
            public void onTick(long millisUntilFinished) {
                txtCountDown.setText("" + (millisUntilFinished - 1000) / 1000);

            }

            @Override
            public void onFinish() {
                showExercise();
            }
        }.start();


    }

    private void showExercise() {
        if (ex_id < exerciseList.size())//list size contains all exercises
        {
            ex_img.setVisibility(View.VISIBLE);
            btnStart.setVisibility(View.VISIBLE);
            layoutGetReady.setVisibility(View.INVISIBLE);

            if (yogaDatabase.getSettingMode() == 0) {
                easyCountDownTimer.start();
            } else if (yogaDatabase.getSettingMode() == 1) {
                mediumCountDownTimer.start();
            } else if (yogaDatabase.getSettingMode() == 2) {
                hardCountDownTimer.start();
            }


            //set data
            ex_img.setImageBitmap(exerciseList.get(ex_id).getImage());
            ex_name.setText(exerciseList.get(ex_id).getName());
        } else {
            showFinished();
        }
    }

    private void showFinished() {
        ex_img.setVisibility(View.INVISIBLE);
        btnStart.setVisibility(View.INVISIBLE);
        timer.setVisibility(View.INVISIBLE);
        layoutGetReady.setVisibility(View.VISIBLE);
        progressBar.setVisibility(View.INVISIBLE);

        txtGetReady.setText("Finished !!!");
        txtCountDown.setText("Congratulation \n you  done today exercise");
        txtCountDown.setTextSize(20);
        String value = String.valueOf(Calendar.getInstance().getTimeInMillis());
// save workout to database
        yogaDatabase.saveDay(value);
        Log.d("log", value);
    }

    private void setExerciseInformation(int id) {
        ex_img.setImageBitmap(exerciseList.get(id).getImage());
        ex_name.setText(exerciseList.get(id).getName());
        btnStart.setText("start");

        ex_img.setVisibility(View.VISIBLE);

        btnStart.setVisibility(View.VISIBLE);
        timer.setVisibility(View.VISIBLE);


        layoutGetReady.setVisibility(View.INVISIBLE);
    }

    private void setExerciseList() {
        String db = "";
        level = getIntent().getStringExtra("level");
        yogaDatabase = new YogaDatabase(this);
        if (level.equals("beginner")) {
            db = "beginner";
        } else if (level.equals("intermediate")) {
            db = "intermediate";
        } else {
            db = "advanced";
        }

        // list = database.getAllExercise(db);
        exerciseList = yogaDatabase.getAllExercise(db);

    }

}

