package com.example.yogatraining;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;

import androidx.appcompat.app.AppCompatActivity;

public class ExerciseLevel extends AppCompatActivity {
    private ImageView imgB;
    private ImageView imgI;
    private ImageView imgA;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_exercise_level);
        imgB = findViewById(R.id.imgB);
        imgI = findViewById(R.id.imgI);
        imgA = findViewById(R.id.imgA);

        View.OnClickListener onClickListener = new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(ExerciseLevel.this, ExerciseList.class);
                switch (v.getId()) {
                    case R.id.imgB:
                        intent.putExtra("level", "beginner");
                        break;
                    case R.id.imgI:
                        intent.putExtra("level", "intermediate");
                        break;
                    case R.id.imgA:
                        intent.putExtra("level", "advanced");
                        break;
                }
                startActivity(intent);

            }
        };

        imgB.setOnClickListener(onClickListener);
        imgI.setOnClickListener(onClickListener);
        imgA.setOnClickListener(onClickListener);
    }
}
