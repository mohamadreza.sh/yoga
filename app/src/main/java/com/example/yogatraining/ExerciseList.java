package com.example.yogatraining;

import android.annotation.SuppressLint;
import android.app.ActionBar;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;

import adapter.ExerciseAdapter;
import database.YogaDatabase;
import model.Exercise;
import technolifestyle.com.imageslider.FlipperLayout;
import technolifestyle.com.imageslider.FlipperView;

public class ExerciseList extends AppCompatActivity {
    RecyclerView recyclerView;
    RecyclerView.LayoutManager layoutManager;
    ExerciseAdapter exerciseAdapter;
    YogaDatabase database;
    ArrayList<Exercise> list;
    FlipperLayout flipperLayout;
    FlipperView flipper;
    String level;
    TextView textView;
    ImageButton imageButton;
    Toolbar toolbar;
    ActionBar actionBar;


    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }

    @SuppressLint("RestrictedApi")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_exercise_list);
        level = getIntent().getStringExtra("level");
        toolbar = findViewById(R.id.toolbar);

        toolbar.setTitle(level);
        toolbar.setTitleTextAppearance(this, R.style.TollbarStyle);
        //setSupportActionBar(toolbar);
        toolbar.setNavigationIcon(R.drawable.backpress);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(ExerciseList.this, "back", Toast.LENGTH_SHORT).show();
            }
        });
        setSupportActionBar(toolbar);


        // getSupportActionBar().setTitle(level);


        database = new YogaDatabase(this);
        recyclerView = findViewById(R.id.list_ex);
        flipperLayout = findViewById(R.id.flipper);


        int[] images = {R.drawable.yogam, R.drawable.yogan, R.drawable.yogat};
        for (int image : images) {
            flipper = new FlipperView(getBaseContext());
            flipper.setImageDrawable(image);
            flipperLayout.addFlipperView(flipper);
        }


        setExerciseList();


    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            onBackPressed();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    private void setExerciseList() {
        String db = "";
        list = new ArrayList<>();

        if (level.equals("beginner")) {
            db = "beginner";
        } else if (level.equals("intermediate")) {
            db = "intermediate";
        } else {
            db = "advanced";
        }

        list = database.getAllExercise(db);

        recyclerView.setHasFixedSize(true);
        layoutManager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(layoutManager);

        exerciseAdapter = new ExerciseAdapter(list, getBaseContext());
        recyclerView.setAdapter(exerciseAdapter);

    }

}
