package com.example.yogatraining;

import android.os.Bundle;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.material.appbar.AppBarLayout;

import java.util.ArrayList;

import adapter.FavExerciseAdapter;
import database.YogaDatabase;
import model.Exercise;
import technolifestyle.com.imageslider.FlipperLayout;

public class FavExercise extends AppCompatActivity {
    RecyclerView recyclerView;
    RecyclerView.LayoutManager layoutManager;
    FavExerciseAdapter exerciseAdapter;
    ArrayList<Exercise> exerciseList;
    FlipperLayout flipperLayout;
    AppBarLayout appBarLayout;
    RelativeLayout relativeLayout;
    TextView txtStatus;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_exercise_list);
        flipperLayout = findViewById(R.id.flipper);
        appBarLayout = findViewById(R.id.appbar);
        flipperLayout.setVisibility(View.GONE);
        appBarLayout.setVisibility(View.GONE);
        relativeLayout = findViewById(R.id.txtRelative);
        txtStatus = findViewById(R.id.txtStatus);


        recyclerView = findViewById(R.id.list_ex);
        recyclerView.setHasFixedSize(true);
        layoutManager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(layoutManager);
        exerciseList = new ArrayList<>();


        YogaDatabase database = new YogaDatabase(this);
        exerciseList = database.getFavExercise();
        if (exerciseList.isEmpty()) {
            relativeLayout.setVisibility(View.VISIBLE);
            txtStatus.setVisibility(View.VISIBLE);
        }
        exerciseAdapter = new FavExerciseAdapter(exerciseList, getBaseContext());
        recyclerView.setAdapter(exerciseAdapter);

    }
}
