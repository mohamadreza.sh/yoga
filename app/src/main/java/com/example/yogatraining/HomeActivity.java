package com.example.yogatraining;

import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

public class HomeActivity extends AppCompatActivity {
    private ImageView imgCalender;
    private ImageView imgSetting;
    private ImageView imgExercise;
    private ImageView imgStart;
    private ImageView imgGallery;
    private ImageView imgFav;
    Context context;
    Resources resources;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.design);
        imgCalender = findViewById(R.id.imgCalender);
        imgSetting = findViewById(R.id.imgSetting);
        imgExercise = findViewById(R.id.imgExercise);
        imgStart = findViewById(R.id.imgStart);
        imgGallery = findViewById(R.id.imgGallery);
        imgFav = findViewById(R.id.imgFav);


//        ActionBar actionBar;
//        actionBar = getSupportActionBar();
//
//        ColorDrawable colorDrawable
//                = new ColorDrawable(Color.parseColor("#FF6200EE"));
//        assert actionBar != null;
//        actionBar.setBackgroundDrawable(colorDrawable);

//        if (LocaleHelper.getLanguage(getApplicationContext()).length() != 0) {
//            context = LocaleHelper.setLocale(HomeActivity.this, LocaleHelper.getLanguage(getApplicationContext()));
//        } else {
//            context = LocaleHelper.setLocale(HomeActivity.this, "en");
//        }
//        resources = context.getResources();
//        txtCalender.setText(resources.getString(R.string.Calender_name));
//        txtSetting.setText(resources.getString(R.string.Setting_name));
//        txtExercise.setText(resources.getString(R.string.Exercise_name));
//        txtFavorite.setText(resources.getString(R.string.Favorite_name));


        View.OnClickListener onClickListener = new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                switch (v.getId()) {
                    case R.id.imgCalender:
                        startActivity(new Intent(HomeActivity.this, Calender.class));
                        break;
                    case R.id.imgSetting:
                        startActivity(new Intent(HomeActivity.this, SettingPage.class));
                        break;
                    case R.id.imgExercise:
                        startActivity(new Intent(HomeActivity.this, ExerciseLevel.class));
                        break;
                    case R.id.imgStart:
                        startActivity(new Intent(HomeActivity.this, Daily_Training_level.class));
                        break;
                    case R.id.imgGallery:
                        startActivity(new Intent(HomeActivity.this, MainActivity.class));
                        break;
                    case R.id.imgFav:
                        startActivity(new Intent(HomeActivity.this, FavExercise.class));
                        break;
                }
            }
        };

        imgCalender.setOnClickListener(onClickListener);
        imgSetting.setOnClickListener(onClickListener);
        imgExercise.setOnClickListener(onClickListener);
        imgStart.setOnClickListener(onClickListener);
        imgGallery.setOnClickListener(onClickListener);
        imgFav.setOnClickListener(onClickListener);


    }

//    @Override
//    public boolean onCreateOptionsMenu(Menu menu) {
//        getMenuInflater().inflate(R.menu.menu, menu);
//
//        return true;
//    }
//
//    @Override
//    public boolean onOptionsItemSelected(MenuItem item) {
//        if (item.getItemId() == R.id.language_en) {
//            context = LocaleHelper.setLocale(this, "en");
//
//        } else if (item.getItemId() == R.id.language_de) {
//            context = LocaleHelper.setLocale(this, "de");
//
//
//        }
//        resources = context.getResources();
//        txtCalender.setText(resources.getString(R.string.Calender_name));
//        txtSetting.setText(resources.getString(R.string.Setting_name));
//        txtExercise.setText(resources.getString(R.string.Exercise_name));
//        txtFavorite.setText(resources.getString(R.string.Favorite_name));
//        return true;
//    }
}
