package com.example.yogatraining;

import android.os.Bundle;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;
import java.util.List;

import adapter.ShowVideoAdapter;
import model.DataList;


public class MainActivity extends AppCompatActivity {
    RecyclerView recyclerView;
    List<DataList> data = new ArrayList<>();


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        setData();
        recyclerView = findViewById(R.id.showVideoRecyclerView);
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        ShowVideoAdapter adapter = new ShowVideoAdapter(data, this);
        recyclerView.setAdapter(adapter);

    }


    private void setData() {
        data.add(new DataList("https://www.youtube.com/embed/Yi0st0T8HJs"));
        data.add(new DataList("https://www.youtube.com/embed/q_F4Vdd6lsE"));
        data.add(new DataList("https://www.youtube.com/embed/3WJam-pECBo"));
        data.add(new DataList("https://www.youtube.com/embed/ayRU3ecmPbI"));
        data.add(new DataList("https://www.youtube.com/embed/zawnfLP0gRE"));
        data.add(new DataList("https://www.youtube.com/embed/zawnfLP0gRE"));
//        data.add(new DataList("<iframe width=\"400\" height=\"200\" src=\"https://www.youtube.com/embed/Yi0st0T8HJs?list=PLU-EPNIaWlfQCzcHonXB-CSijqZxWJsND\" frameborder=\"0\" allow=\"accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture\" allowfullscreen></iframe>"));
//        data.add(new DataList("<iframe width=\"100\" height=\"100\" src=\"https://www.youtube.com/embed/q_F4Vdd6lsE?list=PLU-EPNIaWlfQCzcHonXB-CSijqZxWJsND\" frameborder=\"0\" allow=\"accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture\" allowfullscreen></iframe>"));
//        data.add(new DataList("<iframe width=\"100\" height=\"100\" src=\"https://www.youtube.com/embed/zawnfLP0gRE?list=PLU-EPNIaWlfQCzcHonXB-CSijqZxWJsND\" frameborder=\"0\" allow=\"accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture\" allowfullscreen></iframe>"));
    }


}
