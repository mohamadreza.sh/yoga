package com.example.yogatraining;

import android.annotation.SuppressLint;
import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TimePicker;
import android.widget.Toast;
import android.widget.ToggleButton;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;
import androidx.cardview.widget.CardView;

import java.util.Calendar;

import database.YogaDatabase;

public class SettingPage extends AppCompatActivity {
    CardView cardView;
    RadioGroup radioGroup;
    RadioButton radioEasy;
    RadioButton radioMedium;
    RadioButton radioHard;
    ToggleButton switchAlarm;
    TimePicker timePicker;
    Button btnSave;
    YogaDatabase yogaDatabase;
    int notificationId = 1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_setting_page);

        cardView = findViewById(R.id.cardView);
        radioGroup = findViewById(R.id.radioGroup);
        radioEasy = findViewById(R.id.radioEasy);
        radioMedium = findViewById(R.id.radioMedium);
        radioHard = findViewById(R.id.radioHard);
        switchAlarm = findViewById(R.id.switchAlarm);
        timePicker = findViewById(R.id.timePicker);
        btnSave = findViewById(R.id.btnSave);
        yogaDatabase = new YogaDatabase(this);
        int mode = yogaDatabase.getSettingMode();
        setRadioButton(mode);


        btnSave.setOnClickListener(new View.OnClickListener() {
            @RequiresApi(api = Build.VERSION_CODES.M)
            @Override
            public void onClick(View v) {
                saveWorkoutMode();
                saveAlarm(switchAlarm.isChecked());
                Toast.makeText(SettingPage.this, "saved", Toast.LENGTH_SHORT).show();
                finish();
            }
        });


    }

    @SuppressLint("ShortAlarm")
    @RequiresApi(api = Build.VERSION_CODES.M)
    private void saveAlarm(boolean checked) {

        if (checked) {
            Intent intent;
            PendingIntent pendingIntent;

            intent = new Intent(SettingPage.this, AlarmNotificationReceiver.class);
            intent.putExtra("notificationId", notificationId);
            pendingIntent = PendingIntent.getBroadcast(this, 0, intent, PendingIntent.FLAG_CANCEL_CURRENT);
            AlarmManager manager = (AlarmManager) getSystemService(ALARM_SERVICE);


            //set time to alarm

            int hour = timePicker.getCurrentHour();
            int minute = timePicker.getCurrentMinute();

            Calendar calendar = Calendar.getInstance();
            //  Date date = Calendar.getInstance().getTime();

            // calendar.set(date.getYear(), date.getMonth(), date.getDay(), hour, minute);

            calendar.set(Calendar.HOUR_OF_DAY, hour);
            calendar.set(Calendar.MINUTE, minute);
            calendar.set(Calendar.SECOND, 0);
            long alarmStartTime = calendar.getTimeInMillis();
            manager.set(AlarmManager.RTC_WAKEUP, alarmStartTime, pendingIntent);


            // manager.setRepeating(AlarmManager.RTC_WAKEUP, calendar.getTimeInMillis(), AlarmManager.INTERVAL_DAY, pendingIntent);
            Log.d("debug", "alarm is wake up :" + timePicker.getHour() + ":" + timePicker.getMinute());

        } else {
            //cancel alarm
            Intent intent = new Intent(SettingPage.this, AlarmNotificationReceiver.class);
            PendingIntent pendingIntent = PendingIntent.getBroadcast(this, 0, intent, 0);
            AlarmManager manager = (AlarmManager) getSystemService(Context.ALARM_SERVICE);

            manager.cancel(pendingIntent);
            Log.d("debug", "alarm is not wake up :" + timePicker.getHour() + ":" + timePicker.getMinute());

        }
    }

    private void setRadioButton(int mode) {
        if (mode == 0) {
            radioGroup.check(R.id.radioEasy);
        } else if (mode == 1) {
            radioGroup.check(R.id.radioMedium);
        } else
            radioGroup.check(R.id.radioHard);
    }

    private void saveWorkoutMode() {
        int selectedId = radioGroup.getCheckedRadioButtonId();
        if (selectedId == radioEasy.getId()) {
            yogaDatabase.saveSettingMode(0);
        } else if (selectedId == radioMedium.getId()) {
            yogaDatabase.saveSettingMode(1);
        } else if (selectedId == radioHard.getId()) {
            yogaDatabase.saveSettingMode(2);
        }
    }


}
