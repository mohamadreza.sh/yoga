package com.example.yogatraining;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

public class SpManager {
    SharedPreferences preferences;
    SharedPreferences.Editor editor;

    public SpManager(Context context) {
        preferences = PreferenceManager.getDefaultSharedPreferences(context);
        editor = preferences.edit();
        editor.apply();
    }

    public boolean isChanelCreated() {
        return preferences.getBoolean("chanel_created", false);

    }

    public void setChancelCreated(boolean created) {
        editor.putBoolean("chanel created", created);
        editor.apply();
        editor.commit();
    }

}
