package custom;

import android.content.Context;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;

import androidx.core.content.ContextCompat;

import com.example.yogatraining.R;
import com.prolificinteractive.materialcalendarview.CalendarDay;
import com.prolificinteractive.materialcalendarview.DayViewDecorator;
import com.prolificinteractive.materialcalendarview.DayViewFacade;

import java.util.HashSet;

public class WorkoutDoneDecoder implements DayViewDecorator {
    HashSet<CalendarDay> list;
    ColorDrawable colorDrawable;
    Drawable drawable;

    public WorkoutDoneDecoder(HashSet<CalendarDay> list, Context context) {
        this.list = list;
        // colorDrawable = new ColorDrawable(Color.parseColor("#B6B3B3"));
        drawable = ContextCompat.getDrawable(context, R.drawable.correct);

    }

    @Override
    public boolean shouldDecorate(CalendarDay day) {
        return list.contains(day);
    }

    @Override
    public void decorate(DayViewFacade view) {
        // view.setBackgroundDrawable(colorDrawable);
        view.setSelectionDrawable(drawable);
        // view.setSelectionDrawable(colorDrawable);

    }
}
