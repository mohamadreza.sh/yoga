package database;

import android.annotation.SuppressLint;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteQueryBuilder;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.util.Base64;
import android.widget.Toast;

import com.readystatesoftware.sqliteasset.SQLiteAssetHelper;

import java.io.ByteArrayOutputStream;
import java.util.ArrayList;
import java.util.List;

import model.Exercise;

public class YogaDatabase extends SQLiteAssetHelper {

    private static final String DB_NAME = "Yoga.db";
    private static final int DB_VER = 1;
    Context context;

    public YogaDatabase(Context context) {
        super(context, DB_NAME, null, DB_VER);
    }


    public int getSettingMode() {
        SQLiteDatabase db = getReadableDatabase();
        SQLiteQueryBuilder qb = new SQLiteQueryBuilder();
        String[] sqlSelect = {"Mode"};
        String sqlTable = "Setting";
        qb.setTables(sqlTable);
        Cursor cursor = qb.query(db, sqlSelect, null, null, null, null, null);
        cursor.moveToFirst();
        return cursor.getInt(cursor.getColumnIndex("Mode"));
    }

    public void changeFav(int id, int fav) {
        String command = "update AllExercise set fav=" + fav + " where id=" + id;
        SQLiteDatabase db = this.getWritableDatabase();
        db.execSQL(command);
    }


    public void saveSettingMode(int value) {
        SQLiteDatabase db = getReadableDatabase();
        String query = "UPDATE Setting SET Mode = " + value;
        db.execSQL(query);
    }


    public ArrayList<Exercise> getFavExercise() {
        ArrayList<Exercise> exerciseList = new ArrayList<>();
        String command = "";
        command = "select * from SaveExercise ";

        SQLiteDatabase db = this.getReadableDatabase();
        @SuppressLint("Recycle") Cursor table = db.rawQuery(command, null);
        table.moveToFirst();
        while (!table.isAfterLast()) {
            Exercise exercise = new Exercise();
            exercise.setId(table.getInt(table.getColumnIndex("id")));
            exercise.setName(table.getString(table.getColumnIndex("name")));
            Bitmap img = StringToImage(table.getString(table.getColumnIndex("image")));
            exercise.setImage(img);
            exercise.setFav(table.getInt(table.getColumnIndex("fav")));
            exerciseList.add(exercise);
            table.moveToNext();
        }

        return exerciseList;
    }


    public void saveFavExercise(Exercise exercise) {
        String img = ImageToString(exercise.getImage());
        String command = "insert into SaveExercise (name,image,fav,id) values ('" + exercise.getName() + "','" + img + "'," + exercise.getFav() + "," + exercise.getId() + ")";
        SQLiteDatabase db = this.getWritableDatabase();
        db.execSQL(command);
    }

    public ArrayList<Exercise> getAllExercise(String l) {
        String command;
        if (l.equals("beginner")) {
            command = "select * from AllExercise where  level='beginner'";
        } else if (l.equals("intermediate")) {
            command = "select * from AllExercise where level='intermediate'";
        } else {
            command = "select * from AllExercise where level='advanced'";

        }

        try {

            ArrayList<Exercise> list = new ArrayList<>();

            SQLiteDatabase database = getReadableDatabase();
            if (database != null) {
                Cursor c = database.rawQuery(command, null);
                if (c.getCount() != 0) {
                    while (c.moveToNext()) {
                        byte[] imageEx = c.getBlob(0);
                        String name = c.getString(1);
                        int fav = c.getInt(2);
                        int id = c.getInt(3);
                        String level = c.getString(4);
                        Bitmap image = BitmapFactory.decodeByteArray(imageEx, 0, imageEx.length);
                        list.add(new Exercise(image, name, fav, id, level)
                        );
                    }

                    return list;

                } else {
                    Toast.makeText(context, "no data retriverd", Toast.LENGTH_SHORT).show();
                    return null;
                }
            } else {
                Toast.makeText(context, "data is null", Toast.LENGTH_SHORT).show();
                return null;
            }

        } catch (Exception e) {
//            Toast.makeText(context, "getAllData" + e.getMessage(), Toast.LENGTH_SHORT).show();
            return null;

        }

    }

    public void deleteContact(int id) {
        String command = "delete from SaveExercise where id=" + id;
        SQLiteDatabase db = this.getWritableDatabase();
        db.execSQL(command);
    }

    public List<String> getWorkoutDays() {
        SQLiteDatabase db = getReadableDatabase();
        SQLiteQueryBuilder qb = new SQLiteQueryBuilder();
        String[] sqlSelect = {"Day"};
        String sqlTable = "WorkoutDays";
        qb.setTables(sqlTable);
        Cursor cursor = qb.query(db, sqlSelect, null, null, null, null, null);

        List<String> result = new ArrayList<>();
        if (cursor.moveToFirst()) {
            do {
                result.add(cursor.getString(cursor.getColumnIndex("Day")));

            } while (cursor.moveToNext());


        }
        return result;
    }

    public void saveDay(String value) {
        SQLiteDatabase database = getReadableDatabase();
        String query = String.format("INSERT INTO WorkoutDays (Day) VALUES ('%s')", value);
        database.execSQL(query);
    }

    private String ImageToString(Bitmap bmp) {
        ByteArrayOutputStream stream = new ByteArrayOutputStream();
        bmp.compress(Bitmap.CompressFormat.PNG, 100, stream);
        byte[] imageBytes = stream.toByteArray();
        return Base64.encodeToString(imageBytes, Base64.DEFAULT);
    }

    private Bitmap StringToImage(String str) {
        byte[] decodedString = Base64.decode(str, Base64.DEFAULT);
        return BitmapFactory.decodeByteArray(decodedString, 0, decodedString.length);
    }


}
