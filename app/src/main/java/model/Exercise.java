package model;

import android.graphics.Bitmap;

public class Exercise {
    private Bitmap image;
    private String name;
    private int fav;
    private int id;
    private String level;

    public Exercise() {

    }

    public Exercise(Bitmap image, String name, int fav, int id, String level) {
        this.image = image;
        this.name = name;
        this.fav = fav;
        this.id = id;
        this.level = level;
    }

    public Bitmap getImage() {
        return image;
    }

    public void setImage(Bitmap image) {
        this.image = image;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getFav() {
        return fav;
    }

    public void setFav(int fav) {
        this.fav = fav;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getLevel() {
        return level;
    }

    public void setLevel(String level) {
        this.level = level;
    }
}
